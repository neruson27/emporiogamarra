/** @format */

import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    paddingHorizontal: 15,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    fontSize: 14,
    color: "#2e2e2e",
  },
  selectedText: (text) => ({
    fontWeight: "bold",
    color: text,
  }),
});
