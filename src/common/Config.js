/** @format */

import Images from "./Images";
import Constants from "./Constants";
import Icons from "./Icons";

export default {
 
  ProductSize: {
    enable: true,
  },

  HomeCategories: [
    // {
    //   category: 18,
    //   image: require("@images/categories_icon/ic_shorts.png"),
    //   colors: ["#4facfe", "#00f2fe"],
    // },
    {
      category: 73,
      image: require("@images/categories_icon/ic_dress.png"),
      colors: ["#7F00FF", "#E100FF"],
    },
    {
      category: 72,
      image: require("@images/categories_icon/ic_tshirt.png"),
      colors: ["#43e97b", "#38f9d7"],
    },
    // {
    //   category: 208,
    //   image: require("@images/categories_icon/ic_panties.png"),
    //   colors: ["#fa709a", "#fee140"],
    // },
    {
      category: 71,
      image: require("@images/categories_icon/ic_woman_shoes.png"),
      colors: ["#f43b47", "#453a94"],
    },
    {
      category: 70,
      image: require("@images/categories_icon/ic_glasses.png"),
      colors: ["#30cfd0", "#330867"],
    },
    {
      category: 74,
      image: require("@images/categories_icon/ic_shoes.png"),
      colors: ["#2af598", "#009efd"],
    },
    {
      category: 76,
      image: require("@images/categories_icon/ic_socks.png"),
      colors: ["#c471f5", "#fa71cd"],
    },
  ],
  
  Payments: {
    cod: require("@images/payment_logo/cash_on_delivery.png"),
    paypal: require("@images/payment_logo/PayPal.png"),
    stripe: require("@images/payment_logo/stripe.png"),
  },

  
  shipping: {
    visible: true,
    zoneId: 1,
    time: {
      free_shipping: "4 - 7 Days",
      flat_rate: "1 - 4 Days",
      local_pickup: "1 - 4 Days",
    },
  },
  showStatusBar: true,
  LogoImage: require("@images/logo-main.png"),
  LogoWithText: require("@images/logo_with_text.png"),
  LogoLoading: require("@images/Spin.png"),

  showAdmobAds: false,
  AdMob: {
    deviceID: "pub-2101182411274198",
    unitID: "ca-app-pub-2101182411274198/4100506392",
    unitInterstitial: "ca-app-pub-2101182411274198/8930161243",
    isShowInterstital: true,
  },
  appFacebookId: "309648023261509",
  CustomPages: { contact_id: 10941 },
  WebPages: { marketing: "http://neruson27.github.com" },

  intro: [
    {
      key: "page1",
      title: "Comprar con nosotros es muy facil!",
      text:
        "Con un simple toque puedes llevar tus articulos a tu casa, de forma muy rapida y sencilla!.",
      icon: "ios-basket-outline",
      colors: ["#0FF0B3", "#036ED9"],
    },
    {
      key: "page2",
      title: "Pagos 100% seguros",
      text:
        "No temas en pagar con nosotros, usamos las mejores pasarelas de pagos y te garantizamos tu dinero!",
      icon: "ios-card-outline",
      colors: ["#13f1fc", "#0470dc"],
    },
    {
      key: "page3",
      title: "Seguro y Facil",
      text: "Que esperas! compra con nosotros!",
      icon: "ios-finger-print-outline",
      colors: ["#b1ea4d", "#459522"],
    },
  ],

  /**
   * Config For Left Menu Side Drawer
   * @param goToScreen 3 Params (routeName, params, isReset = false)
   * BUG: Language can not change when set default value in Config.js ==> pass string to change Languages
   */
  menu: {
    // has child categories
    isMultiChild: true,
    // Unlogged
    listMenuUnlogged: [
      {
        text: "Login",
        routeName: "LoginScreen",
        params: {
          isLogout: false,
        },
        icon: Icons.MaterialCommunityIcons.SignIn,
      },
    ],
    // user logged in
    listMenuLogged: [
      {
        text: "Logout",
        routeName: "LoginScreen",
        params: {
          isLogout: true,
        },
        icon: Icons.MaterialCommunityIcons.SignOut,
      },
    ],
    // Default List
    listMenu: [
      {
        text: "Shop",
        routeName: "Home",
        icon: Icons.MaterialCommunityIcons.Home,
      },
      {
        text: "News",
        routeName: "NewsScreen",
        icon: Icons.MaterialCommunityIcons.News,
      },
      {
        text: "contactus",
        routeName: "CustomPage",
        params: {
          id: 10941,
          title: "contactus",
        },
        icon: Icons.MaterialCommunityIcons.Pin,
      },
      {
        text: "About",
        routeName: "CustomPage",
        params: {
          url: "http://emporiogamarra.com.pe/",
        },
        icon: Icons.MaterialCommunityIcons.Email,
      },
      // {
      //   text: "Setting",
      //   routeName: "SettingScreen",
      //   icon: Icons.MaterialCommunityIcons.Setting,
      // },
    ],
  },

  // define menu for profile tab
  ProfileSettings: [
    {
      label: "WishList",
      routeName: "WishListScreen",
    },
    {
      label: "MyOrder",
      routeName: "MyOrders",
    },
    {
      label: "Address",
      routeName: "Address",
    },
    // {
    //   label: "Currency",
    //   isActionSheet: true,
    // },
    // {
    //   label: "DarkTheme",
    // },
    {
      label: "contactus",
      routeName: "CustomPage",
      params: {
        id: 10941,
        title: "contactus",
      },
    },
    {
      label: "Privacy",
      routeName: "CustomPage",
      params: {
        url: "https://emporiogamarra.com.pe/privacy",
      },
    },
    {
      label: "termCondition",
      routeName: "CustomPage",
      params: {
        url: "https://emporiogamarra.com.pe/term-of-service",
      },
    },
    {
      label: "About",
      routeName: "CustomPage",
      params: {
        url: "http://emporiogamarra.com.pe",
      },
    },
  ],

  // Layout select
  layouts: [
    {
      layout: Constants.Layout.card,
      image: Images.icons.iconCard,
      text: "cardView",
    },
    {
      layout: Constants.Layout.simple,
      image: Images.icons.iconRight,
      text: "simpleView",
    },
    {
      layout: Constants.Layout.twoColumn,
      image: Images.icons.iconColumn,
      text: "twoColumnView",
    },
    {
      layout: Constants.Layout.threeColumn,
      image: Images.icons.iconThree,
      text: "threeColumnView",
    },
    {
      layout: Constants.Layout.horizon,
      image: Images.icons.iconHorizal,
      text: "horizontal",
    },
    {
      layout: Constants.Layout.advance,
      image: Images.icons.iconAdvance,
      text: "advanceView",
    },
  ],

  // Default theme loading, this could able to change from the user profile (reserve feature)
  Theme: {
    isDark: false,
  },

  // new list category design
  CategoriesLayout: Constants.CategoriesLayout.column,

  DefaultCurrency: {
    symbol: "S/",
    name: "Sol",
    code: "Sol",
    name_plural: "Soles",
    decimal: ".",
    thousand: ",",
    precision: 2,
    format: "%s%v", // %s is the symbol and %v is the value
  },

  DefaultCountry: {
    code: "pe",
    RTL: false,
    language: "Spanish",
    countryCode: "PE",
    hideCountryList: false, // when this option is try we will hide the country list from the checkout page, default select by the above 'countryCode'
  },

  /**
   * Config notification onesignal, only effect for the Pro version
   */
  OneSignal: {
    appId: "7e3e8a17-820b-4cff-82ad-0c14e5f830da",
  },
  /**
   * Login required
   */
  Login: {
    RequiredLogin: false, // required before using the app
    AnonymousCheckout: false, // required before checkout or checkout anonymous
  },

  Layout: {
    HideHomeLogo: false,
    HideLayoutModal: false
  },

  Affiliate: { enable: false }
};
