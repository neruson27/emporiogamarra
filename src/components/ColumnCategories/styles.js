import React, { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");
// const widthItem = (width - 10) / 2
const widthItem = (width - 10)

export default StyleSheet.create({
  item: {
    width: widthItem,
    height: (widthItem - 10) / 2,
    marginTop: 10,
    borderRadius: 10
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    borderRadius: 10
  },
  content: {
    ...StyleSheet.absoluteFill,
    paddingHorizontal: 5,
    borderRadius: 10
  },
  wrap: {
    flex: 1,
    backgroundColor: 'rgba(155,155,155,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10
  },
  name: {
    color: "#fff",
    fontSize: 18
  },
  list: {
    flexDirection: 'column',
    paddingHorizontal: 5,
    paddingBottom: 10,
    borderRadius: 10
  }
});
