/** @format */

export default {
  Exit: "Salir",
  ExitConfirm: "¿Estas seguro que quieres salir de la app?",
  YES: "Si",
  OK: "OK",
  ViewMyOrders: "Mis ordenes",
  CANCEL: "Cancelar",
  Confirm: "Confirmar",

  // Scene's Titles
  Home: "Home",
  Intro: "Introduccion",
  Product: "Productos",
  Cart: "Carrito",
  WishList: "WishList",

  // Home
  products: "productos",

  // TopBarz
  ShowFilter: "Sub Categorias",
  HideFilter: "Ocultar",
  Sort: "Ordenar",
  textFilter: "Recientes",

  // Category
  ThereIsNoMore: "Aqui no hay productos para mostrar",

  // Product
  AddtoCart: "Añadir al carrito",
  AddtoWishlist: "Añadir a la lista de deseados",
  ProductVariations: "Variaciones",
  NoVariation: "Este producto no tiene Variacion",
  AdditionalInformation: "Descripcion",
  NoProductDescription: "No hay descripcion",
  ProductReviews: "Reseñas",
  NoReview: "Este producto no tiene reseñas... aun",
  BUYNOW: "Comprar ya!",
  OutOfStock: "Fuera de Stock",
  ProductLimitWaring: "No puedes añadir mas de (num) productos",
  EmptyProductAttribute: "Este producto no tiene atributos",
  ProductFeatures: "Detalles",
  ErrorMessageRequest: "No se puede obtener la data del server",
  NoConnection: "Sin conexion a internet",
  ProductRelated: "Relacionados",

  // Cart
  NoCartItem: "No hay productos en el carro",
  Total: "Total",
  EmptyCheckout: "Lo siento no puedes checkear el carro vacio",
  RemoveCartItemConfirm: "Quitar este producto del carro?",
  MyCart: "Carrito",
  Order: "Orden",
  ShoppingCart: "Carrito de compra",
  ShoppingCartIsEmpty: "Tu carrito esta vacio",
  AddProductToCart: "Añadir un producto al carrito",
  TotalPrice: "Precio Total:",
  YourDeliveryInfo: "Detalles de envio",
  ShopNow: "Comprar ahora",
  YourChoice: "Tu carrito:",
  YourSale: "Tu venta:",
  SubtotalPrice: "Precio Sub total:",
  BuyNow: "Pagar ahora",
  Items: "items",
  Item: "item",
  ThankYou: "Gracias",
  FinishOrderCOD: "Puedes usar tu numero de orden para seguir el paquete",
  FinishOrder:
    "Muchas gracias por su compra. Para verificar el estado de su entrega, vaya a Mis pedidos.",
  NextStep: "Siguiente",
  ConfirmOrder: "Confirmar orden",
  RequireEnterAllFileds: "Por favor mete todos los campos",
  Error: "Error",
  InvalidEmail: "Correo invalido",
  Finish: "Final",

  // Wishlist
  NoWishListItem: "No hay ningún artículo en la lista de deseos.",
  MoveAllToCart: "Añadir todo al carrito.",
  EmptyWishList: "lista de deseos vacía",
  EmptyAddToCart: "Lo sentimos, la lista de deseos está vacía.",
  RemoveWishListItemConfirm: "¿Eliminar este producto de la lista de deseos?",
  CleanAll: "Limpiar todo",

  // Sidemenu
  SignIn: "Iniciar sesion",
  SignOut: "Salir",
  GuestAccount: "Cuenta de invitado",
  CantReactEmailError:
    "No pudimos conseguir tu correo electronico, por favor utiliza otro metodo de login",
  NoEmailError: "Tu cuenta no posee una direccion de correo valida",
  EmailIsNotVerifiedError:
    "Tu direccion de correo no esta verificada",
  Login: "Iniciar sesion",
  Logout: "Salir",
  Shop: "Tienda",
  Category: "Categoria",

  // Checkout
  Checkout: "revisar",
  ProceedPayment: "Proceder al pago",
  Purchase: "Compra",
  CashOnDelivery: "Contra reembolso",
  CreditCard: "Tarjeta de credito",
  PaymentMethod: "Metodo de pago",
  PaymentMethodError: "Porfavor selecciona tu metodo de pago",
  PayWithCoD: "Tu compra sera pagada cuando los bienes sean recibidos",
  PayWithPayPal: "Tu comprar sera pagado usando paypal",
  Paypal: "paypal",
  Stripe: "stripe",
  PayWithStripe: "Tu compra sera pagada usando stripe",
  ApplyCoupon: "Aplicar",
  CouponPlaceholder: "Codigo de cupon",
  Apply: "Aplicar",
  Applying: "Aplicando",
  Back: "Regresar",
  CardNamePlaceholder: "Nombre escrito en la tarjeta",
  BackToHome: "Regresar al inicio",
  OrderCompleted: "Tu orden fue completada",
  OrderCanceled: "Tu orden fue cancelada",
  OrderFailed: "Algo malo ocurrio...",
  OrderCompletedDesc: "El id de tu orden es",
  OrderCanceledDesc:
    "Has cancelado tu orden. La orden no ha sido completada",
  OrderFailedDesc:
    "Hemos encontrado un error al momento de procesar tu orden. La transaccion no ha sido coompletada. Porfavor intenta otra vez",
  OrderTip:
    'Tip: Puedes saber el estatus de tu orden en la seccion "Mis ordenes" del menu',
  Delivery: "Entrega",
  Payment: "Pago",
  Complete: "Completar",
  EnterYourFirstName: "Introduce tu primer nombre",
  EnterYourLastName: "Introduce tu apellido",
  EnterYourEmail: "Introduce tu correo",
  EnterYourPhone: "Introduce tu telefono",
  EnterYourAddress: "Introduce tu direccion",
  CreateOrderError: "No se puede crear una nueva orden. Porfavor intente luego",

  // myorder
  OrderId: "Id de la orden",
  MyOrder: "Mi orden",
  NoOrder: "No tienes orden disponible",
  OrderDate: "Fecha de la orden: ",
  OrderStatus: "estatus: ",
  OrderPayment: "Metodo de pago: ",
  OrderTotal: "Total: ",
  OrderDetails: "Mostrar detalles",
  OrderLineItems: "Articulos de linea: ",
  ShippingAddress: "Direccion de envio:",
  Refund: "Reembolso",

  News: "Nuevo",
  PostDetails: "Detalles del post",
  FeatureArticles: "Caraceristicas del articulo",
  MostViews: "Mas vistos",
  EditorChoice: "Eleccion del editor",

  // settings
  Settings: "Configuracion",
  BASICSETTINGS: "Configuraciones basicas",
  Language: "Languaje",
  INFO: "INFO",
  About: "Acerca de nosotros",

  // language
  AvailableLanguages: "Lenguajes disponibles",
  SwitchLanguage: "Cambiar lenguaje",
  SwitchLanguageConfirm: "Cambiar el lenguaje requiere que la app se recargue, desea continuar continuar?",

  // about us
  AppName: "tienda",
  AppDescription: "Plantilla react native para mCommerce",
  AppContact: " Contactanos al: store.io",
  AppEmail: " Correo: support@store.io",
  AppCopyRights: "© store 2016",

  // contact us
  contactus: "Contactanos",

  // form
  NotSelected: "No seleccionado",
  EmptyError: "Este campo esta vacio",
  DeliveryInfo: "Informacion del envio",
  FirstName: "Nombre",
  LastName: "Apellido",
  Address: "Direccion",
  City: "Pueblo/Ciudad",
  State: "Estado",
  NotSelectedError: "Porfavor selecciona uno",
  Postcode: "Codigo postal",
  Country: "Pais",
  Email: "Correo",
  Phone: "Numero de telefono",
  Note: "Nota",

  // search
  Search: "Buscar",
  SearchPlaceHolder: "Buscar producto por su nombre",
  NoResultError: "Tu busqueda no arrojo ningun resultado.",
  Details: "Detalles",

  // filter panel
  Categories: "Categorias",
  Loading: "Cargando...",
  welcomeBack: "Bienvenido! ",
  seeAll: "Mostra todo",

  // Layout
  cardView: "Carta",
  simpleView: "Vista de lista",
  twoColumnView: "Dos columnas",
  threeColumnView: "Tres columnas",
  listView: "Vista de lista",
  default: "Por defecto",
  advanceView: "Avanzado ",
  horizontal: "Horizontal ",

  couponCodeIsExpired: "Este codigo de cupon expiro",
  invalidCouponCode: "Este codigo d cupon es invalido",
  remove: "Remover",
  applyCouponSuccess: "Felicidades!, codigo de cupon aplicado correctamente",
  reload: "Recargar",

  ShippingType: "Metodo de envio",

  // Place holder
  TypeFirstName: "Escribe tu Nombre",
  TypeLastName: "Escribe tu apellido",
  TypeAddress: "Escribe tu direccion",
  TypeCity: "Escribe tu pueblo o ciudad",
  TypeState: "Escribe tu estado",
  TypeNotSelectedError: "Porfavor seleccione uno",
  TypePostcode: "Escribe tu codigo postal",
  TypeEmail: "Escribe tu correo (Ej. acb@gmail.com)",
  TypePhone: "Ej. (99) 9999-9999",
  TypeNote: "Nota",
  TypeCountry: "Selecciona el pais",
  SelectPayment: "Selecciona el metodo de pago",
  close: "Cerrar",
  noConnection: "No tienes acceso a internet",

  // user profile screen
  AccountInformations: "Informacion de la cuenta",
  PushNotification: "Notificaciones push",
  DarkTheme: "Tema obscuro",
  Privacy: "Politica de privacidad",
  SelectCurrency: "Selecciona la moneda",
  Name: "Nombre",
  Currency: "Moneda",
  Languages: "Languajes",
  Guest: "Invitado",
  FacebookLogin: "Iniciar sesion con facebook",
  Or: "O",
  UserOrEmail: "Nombre de usuario o correo",
  DontHaveAccount: "No tienes cuenta? ",
  accountDetails: "Detalles de la cuenta",
  username: "Nombre de usuario",
  email: "Correo",
  generatePass: "Usar el generador de contraseñas",
  password: "Contraseña",
  signup: "Registrarse",
  profileDetail: "Detalles del perfil",
  firstName: "Nombre",
  lastName: "Apellido",

  // Horizontal
  featureProducts: "Caracteristicas del producto",
  bagsCollections: "Colecciones de bolsas",
  womanBestSeller: "Superventas para mujeres",
  manCollections: "Collecion para hombres",

  // Modal
  Select: "Selecciona",
  Cancel: "Cancelar",

  // review
  vendorTitle: "Vendedor",
  comment: "Dejar un comentario",
  yourcomment: "Tu comentario",
  placeComment:
    "Di algo acerca de tu experiencia o deja un consejo para alguien",
  writeReview: "Reseña",
  thanksForReview:
    "Gracias por la reseña, tu comentario sera verificado por el administrador y sera publicado luego",
  errInputComment: "Porfavor escriba su comentario para enviar",
  errRatingComment: "Porfavor clasifique para poder enviar",
  send: "Enviar",
  termCondition: "Terminos y condiciones",
  Subtotal: "Subtotal",
  Discount: "Descuento",
  Shipping: "Envio",
  Recents: "Recientes",
  Filters: "Filtrar",
  Pricing: "Precio",
  Filter: "Filtro",
  ClearFilter: "Limpiar filtro",
  ProductCatalog: "Producto de catalogo",
  ProductTags: "Etiquetas de productos",
  AddToAddress: "Añadir a la direccion",
  OrderNotes: 'Notas de la orden'
};
