/** @format */

import React, { PureComponent } from "react";
import { View, FlatList, TouchableOpacity, Image, Text } from "react-native";
import { connect } from "react-redux";

import { toast, warn } from "@app/Omni";
import { Languages, Images } from "@common";
import styles from "./styles";
import { ImageCache } from '@components'

class Categories extends PureComponent {

  render() {
    const { categories } = this.props;
    return (
      <FlatList
        data={categories}
        renderItem={this.renderItem}
        numColumns={1}
        contentContainerStyle={styles.list}
      />
    );
  }

  renderItem = ({ item }) => {
    let mediumImg = item.image.src.replace('.png', '-small.png')
    return (
      <TouchableOpacity style={styles.item} onPress={() => this.props.onViewCategory(item)}>
        {!item.image && <ImageCache source={{ uri: mediumImg}} style={styles.image} />}
        {item.image && <Image source={{uri: mediumImg}} style={styles.image} />}
        <View style={styles.content}>
          <View style={styles.wrap}>
            <Text style={styles.name}>{item.name}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  componentDidMount() {
    if (this.props.categories.length == 0) {
      this.props.fetchCategories();
    }
  }
}

Categories.defaultProps = {
  categories: [],
};

const mapStateToProps = (state) => {
  return {
    categories: state.categories.list,
    netInfo: state.netInfo,
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { netInfo } = stateProps;
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/CategoryRedux");

  return {
    ...ownProps,
    ...stateProps,
    fetchCategories: () => {
      if (!netInfo.isConnected) return toast(Languages.noConnection);
      actions.fetchCategories(dispatch);
    },
  };
}

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(Categories);
